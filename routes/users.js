var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var bcrypt = require('bcrypt');
var User = require('../models/user');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/register', function(req, res, next) {
  res.render('register', {
    'title':'Register'
  });
});

router.get('/login', function(req, res, next) {
  res.render('login', {
    'title':'login'
  });
});

router.post('/register', function(req, res, next) {
  //Get Form Values
  var name = req.body.name;
  var email = req.body.email;
  var username = req.body.username;
  var password = req.body.password;
  var password2 = req.body.password2;

  console.log(email);
//check for image field
	if(req.files.profileimage){
		console.log('uploading image...');
		//file info
		var profileImageOriginalName = req.files.profileimage.originalname;

		var profileImageName = req.files.profileimage.name;
		var profileImageMime = req.files.profileimage.mimetype;
		var profileImagePath = req.files.profileimage.path;
		var profileImageExt = req.files.profileimage.extension;
		var profileImageSize = req.files.profileimage.size;
	} else{
		var profileImageName = 'noimage.png';
	}

	//form validation
	req.checkBody('name','Name field is required').notEmpty();
	req.checkBody('email','Email field is required').notEmpty();
	req.checkBody('email','Email not valid').isEmail();
	req.checkBody('password','Password field is required').notEmpty();
	req.checkBody('username','UserName field is required').notEmpty();
	req.checkBody('password2','Passwords do not match').equals(req.body.password);

	//check for errors
	var errors = req.validationErrors();

	if(errors){
		res.render('register', {
			errors: errors,
			name: name,
			email: email,
			username : username,
			password: password,
			password2: password2
		});
	}else{
		var newUser = new User({
			name: name,
			email: email,
			username : username,
			password: password,
			profileimage: profileImageName
		});

		//create user
		User.createUser(newUser,function(err, user){
			if(err) throw err;
			console.log(user);
		});

		//success message
		req.flash('success', 'You are now registered and may login');

		//redirect to home page
		res.location('/');
		res.redirect('/');
	}

});

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
	User.getUserById(id, function(err, user){
		done(err, user);
	});
 // done(null, user);
});


passport.use(new LocalStrategy(function(username, password, done){
	
	User.getUserByUsername(username, function(err, user){
	
		if(err) throw err;
		if(!user){
			console.log('Unknown user');
			return done(null, false, {message:'Unknown User'});
		}
		
		if (bcrypt.compareSync(password, user.password)) { 
			console.log('invalid password');
			return done(null, false); 
		}
     	return done(null, user);
     	/*Other way to verify password. Though not workign right now. Check once in vedio
	User.comparePassword(password, user.password, function(err, isMatch){
			console.log('in comare password methodService');
			if(err) throw err;
			if(isMatch){
				return done(null, user);
			}
			else{
				console.log('Invalid password');
				return done(null, false, {message:'Invalid password'});
			}
		});*/
	});
}));

router.post('/login', passport.authenticate('local',{ //Here 'local' means your local database.
	failureRedirect: '/users/login',
	failureFlash: 'Invalid username or password.' // failure message
	}), function(req, res){
	//this will be executed if local comes true. user enters correct password and userid
		console.log('Authentication Successful');
		req.flash('success', 'You are logged in');
		res.redirect('/');
	}
);

router.get('/logout', function (req, res){
	req.logout();
	req.flash('success', 'You have successfully logged out');
	res.redirect('/users/login');
  /*req.session.destroy(function (err) {
    res.redirect('/'); //Inside a callback… bulletproof!
  });*/
});
module.exports = router;
