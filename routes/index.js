var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', ensureAuthenticated, function(req, res, next) {
  res.render('index',{
  	'title':'member'
  });
});

function ensureAuthenticated(req, res, next){
	if(req.isAuthenticated()){   //if user is looged in, req.isAuthenticated() will return true 
		return next();
	}
	res.redirect('users/login');
}

module.exports = router;
